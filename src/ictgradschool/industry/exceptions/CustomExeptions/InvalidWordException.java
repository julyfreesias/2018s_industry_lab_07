package ictgradschool.industry.exceptions.CustomExeptions;

public class InvalidWordException extends Exception {
    public InvalidWordException (String message){
        super(message);
    }
}
