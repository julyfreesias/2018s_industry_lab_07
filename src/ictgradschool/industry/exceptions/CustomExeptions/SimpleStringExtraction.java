package ictgradschool.industry.exceptions.CustomExeptions;

import ictgradschool.Keyboard;
import ictgradschool.industry.exceptions.arraysandexceptions.InvalidIndexException;

public class SimpleStringExtraction {

    private void getWordFromUser() throws ExceedMaxStringLengthException, InvalidWordException {
        System.out.println("Enter a string of at most 100 characters :");

        String word = Keyboard.readInput();
        if (word.length() > 100) {
            throw new ExceedMaxStringLengthException("String is more than 100 characters!");
        }else {

            String[] splitwords= word.split(" ");
            String extractedWords = "";
            for (int i =0; i<splitwords.length; i++){
                char c = splitwords[i].charAt(0);
                if(Character.isDigit(c)) {
                    throw new InvalidWordException("It is invlid words.");
                }
                extractedWords += c + " ";
            }
            System.out.println("You entered " + extractedWords);
        }


    }



    public static void main(String[] args) {
        try {
            new SimpleStringExtraction().getWordFromUser();
        } catch (ExceedMaxStringLengthException e) {
            e.printStackTrace();
        } catch (InvalidWordException e) {
            e.printStackTrace();
        }

    }
}
