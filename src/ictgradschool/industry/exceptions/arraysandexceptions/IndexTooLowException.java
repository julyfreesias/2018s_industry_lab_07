package ictgradschool.industry.exceptions.arraysandexceptions;

public class IndexTooLowException extends Exception{
    IndexTooLowException (String message) {
        super(message);
    }
}
