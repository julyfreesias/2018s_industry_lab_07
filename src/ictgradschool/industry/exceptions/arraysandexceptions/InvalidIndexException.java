package ictgradschool.industry.exceptions.arraysandexceptions;

public class InvalidIndexException extends  Exception {
    InvalidIndexException (String message){
        super(message);
    }
}
