package ictgradschool.industry.exceptions.simpleexceptions;

import ictgradschool.Keyboard;

import java.util.Scanner;


public class SimpleExceptions {
    public static void main(String[] args) {

        SimpleExceptions exceptions = new SimpleExceptions();

        //Question 1 & 2
          exceptions.handlingException();

//        Question3
//        exceptions.Question3();
//
//        Question4
//        exceptions.Question4();
    }

    /**
     * The following tries to divide using two user input numbers, but is
     * prone to error.
     */
    public void handlingException() {
        Scanner sc = new Scanner(System.in);


        try {
            System.out.print("Enter the first number: ");
            String str1 = sc.next();
            int num1 = Integer.parseInt(str1);
            System.out.print("Enter the second number: ");
            String str2 = sc.next();
            int num2 = Integer.parseInt(str2);
            System.out.println("The division of " + num1 + " over " + num2 + " is " + (num1 / num2) + "\n");
        } catch (NumberFormatException e) {
             System.out.println(e.getMessage());
        }// Output the result
           catch (ArithmeticException e) {
             System.out.println(e.getMessage());
        }
    }


    public void Question3() {

            String str = "Hello World";
            str.charAt(-1);

// catch (StringIndexOutOfBoundsException e) {
//            System.out.println(e.getMessage());
//        }
        //Write some Java code which throws a StringIndexOutOfBoundsException
    }

    public void Question4() {
        //Write some Java code which throws a ArrayIndexOutOfBoundsException
        String[] words = {"Hello", "World"};
        String w = words[words.length];
    }
}